This repo contains a Dockerfile that can be used to create a lightweight
[Fossil SCM](https://fossil-scm.org/) image.
