FROM ubuntu:bionic

ENV VERSION=2.10
RUN \
        apt-get update && \
        apt-get install -y curl && \
        curl "https://fossil-scm.org/home/uv/fossil-linux-x64-${VERSION}.tar.gz" | tar xvz

FROM scratch
COPY --from=0 /fossil /fossil
ENTRYPOINT ["/fossil"]

